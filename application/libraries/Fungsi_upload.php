<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fungsi_upload{
	
	function __construct(){		
        $this->CI =& get_instance();		
		$this->CI->load->database(); 
		$this->CI->load->library('upload');
	}
	
	function imgupload($param, $path){
		
		#get configuration from config file (upload.php)
		$default = $this->CI->config->load('upload', TRUE);
		#path target uploading file
		$array 	 = array('upload_path'	=> $path);
		#initialize and merge all config files
		$config  = array_merge($default,$array);

		$a=$this->CI->upload->initialize($config);	
			
		#make an exception result
		if(!$this->CI->upload->do_upload($param)){ 
		return FALSE; 
		}		//failed upload	
		else { 
		$temp = $this->CI->upload->file_name;
	
		 return $temp; 
		 }	//success upload
	}#end upload function

	function img_resize($path, $name, $newpath){
				$img_name='img-resize-'.$name;
				$config['image_library']='gd2';
                $config['source_image']=$path.'/'.$name;
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= TRUE;
                $config['quality']= '80%';
                $config['width']	=2000;
                $config['height']	=2000;
                $config['new_image']= $newpath.'/img-resize-'.$name;
                $this->CI->load->library('image_lib', $config);
                $img = $this->CI->image_lib->resize();
                @unlink($path.'/'.$name);
                return $img_name;


}
	function upload_banyak($tempat_gambar){
		$config = array();
		$config['upload_path'] 		= $tempat_gambar;
	    // $config['upload_path'] 		= './Images/post';
	    $config['allowed_types'] 	= 'gif|jpg|png|jpeg';
	    $config['max_size']      	= '0';
	    $config['overwrite']     	= FALSE;
	    
	    return $config;
	}
	
	function imgrezise($name,$path,$newpath){	
		#get configuration from config file (rezize.php)
		$default = $this->CI->config->load('resize', TRUE);	
		#path target uploading file
		$array['source_image']	= $path . $name;
		$array['new_image'] = $path . $name;
		#initialize and merge all config files
		$resize = array_merge($default,$array);
		$this->CI->load->library('image_lib', $resize);
		$this->CI->image_lib->resize();	
		#make an exception result
		if($this->CI->image_lib->resize()){ return $name; }			//success resize
		elseif(!$this->CI->image_lib->resize()){ return FALSE; }	//failed resize		
		else{ return FALSE; }										//failed resize
	}#end resize function
	
} #end of controller
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Templates{
	function __construct(){
        $this->CI =& get_instance();
		$this->CI->load->database();
		date_default_timezone_set('Asia/Jakarta');
    }
	function layout($data){
		$this->CI->load->view('admin_layout/admin_template',$data);
	}
	function model($name,$alias)
	{
		$model =  $this->CI->load->model($name,$alias,TRUE);
		return $model;
	}
	
	function folder()
	{
		$this->CI->load->helper('directory');
		$dir = FCPATH.'templates/';
		$contents 	= scandir($dir);
		$bad 		= array(".", "..", ".DS_Store", "_notes", "Thumbs.db");
		$template 	= array_diff($contents, $bad);
		$default 	= "";
		foreach($template as $t):
			if(is_dir($dir.$t)){
				$default[] = $t; 
			}
		endforeach;
		$dir = $default[0];
		return $dir;
	}
		
	function main($push, $content)
	{	
		#multi language
		if($this->CI->session->userdata('lang')== 'id'){
			$folder = 'id';
		} elseif($this->CI->session->userdata('lang')== 'en'){
			$folder = 'en';
		} else {
			$folder = 'id';
		}
		
		#default folder location
		$template = $folder.'/template';	
		#setup global varibale use for all controller 
		$getLogo = $this->CI->db->get('logo')->row_array();
		if($getLogo){
			$logo = base_url('images/logo/'.$getLogo['gambar']);
		}else{
			$logo = '';
		}
		// $post = $this->CI->db->query("SELECT judul FROM artikel ORDER BY tanggal DESC limit 35")->result();
		// if($post){
		// 	$judul_post = ''; foreach($post as $row){
		// 		$judul_post .= "'".$row->judul."',";
		// 	}
		// } else {
		// 	$judul_post = '';
		// }
		#default array used for view
		$default = array(
			#load element page
			'content'		=> $content,
			#database processing
			'meta'			=> $this->CI->db->order_by('id', 'DESC')->get('seo')->row_array(),
			'logo'			=> $logo,
			// 'post'			=> $judul_post,			
		);		
		#merge and join between default array and array from controller
		$data = array_merge($default, $push);
		#send data to browser
		$this->CI->load->view($template,$data);
	}
	
	function main_branch($push, $content)
	{	
		#multi language
		$folder = 'id';
		#default folder location
		$template = $folder.'/template_branch';	
		#setup global varibale use for all controller 
		$getLogo = $this->CI->db->get('logo')->row_array();
		if($getLogo){
			$logo = base_url('images/logo/'.$getLogo['gambar']);
		}else{
			$logo = '';
		}
		// $post = $this->CI->db->query("SELECT judul FROM artikel ORDER BY tanggal DESC limit 35")->result();
		// if($post){
		// 	$judul_post = ''; foreach($post as $row){
		// 		$judul_post .= "'".$row->judul."',";
		// 	}
		// } else {
		// 	$judul_post = '';
		// }
		#default array used for view
		$default = array(
			#load element page
			'content'		=> $content,
			#database processing
			'meta'			=> $this->CI->db->order_by('id', 'DESC')->get('seo')->row_array(),
			'logo'			=> $logo,
			// 'post'			=> $judul_post,			
		);		
		#merge and join between default array and array from controller
		$data = array_merge($default, $push);
		#send data to browser
		$this->CI->load->view($template,$data);
	}
	
	function admin_chat($push, $content)
	{
		$folder = 'chat';
		$template = $folder.'/chat';	
		#default array used for view
		$default = array(
			#load element page
			'content'		=> $content,
		);		
		#merge and join between default array and array from controller
		$data = array_merge($default, $push);
		$this->CI->load->view($template, $data);
	}
	
	function form($data)
	{	
		$folder = 'id';
		$template = $folder.'/template_form';	
		$this->CI->load->view($template, $data);
	}
}
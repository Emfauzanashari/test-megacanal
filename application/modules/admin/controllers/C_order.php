<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_order extends CI_Controller {
		function __construct()
	{
		parent::__construct();
		# load all model
		$this->load->model('model_order','order', 'TRUE');

	}
	public function index(){

		$data=$this->input->post();


		$insert= array(
		'no_pesanan'		=> rand(),
		'nm_suplier'		=> $data['name'],
		'nm_produk'			=> $data['productName'],
		'qty'				=> $data['qty'],
		'tanggal'			=> $data['tanggal'],
		'total'				=> str_replace('$', '',$data['total']),
		);

		$dbInsert= $this->order->insert($insert);
		if ($dbInsert) {
			$status=1;
			$message='Sukses Input Data';
		}else{
			$status=0;
			$message='Gagal Input Data';

		}

		$output = array(
				'status' 	=> $status,
				'message'	=> $message,
			);

		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($output));
		
	}
	
}//end controller
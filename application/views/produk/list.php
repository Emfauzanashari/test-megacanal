
<!--wrapper-->
<div class="wrapper">
	<div class="container mt-3">
		<div class="row">
			<div class="d-grid gap-2 d-md-flex justify-content-between">
				<div>
					<h2>Product List</h2>
				</div>
				<div>
					<button class="btn btn-primary mb-2" id="btnShowProduct">Show Product</button>
				</div>
			</div>
		</div>
		<div class="spiner">
			<div class="spinner-square d-none" id="loadSpiner">
				<div class="square-1 square"></div>
				<div class="square-2 square"></div>
				<div class="square-3 square"></div>
			</div>	
		</div>

		<div class="row d-none mb-3" id="tableContent">
			<div class="table-responsive">
				<table class="table table-striped" id="tbProduct">
					<thead>
						<tr>
							<th scope="col">Image</th>
							<th scope="col">Title</th>
							<th scope="col">Category</th>
							<th scope="col">Brand</th>
							<th scope="col">Stock</th>
							<th scope="col">Price</th>
							<th scope="col">Action</th>

						</tr>
					</thead>
					<tbody>

					</tbody>
				</table>
			</div>	
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalDetail" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
					<div class="row">
						<div class="col-sm-6">
								<h4 class = "product-title" id="nameProduct"></h4>
							<div class = "product-imgs">
								<div class = "img-display">
									<div class = "img-showcase" id="showCase"></div>
								</div>
								<div class = "img-select" id="imgSelect"></div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class = "product-content">

								<div class = "product-price d-md-flex justify-content-between">
									<p class = "new-price">New Price: <span id="price">$</span></p>
									  <div class="rating-star">
                                        <span class="rating-star star_small" data-starnum="" id="starRating"><i></i></span><span id="rating"></span>
                                    </div>
								</div>

								<div class = "product-price d-md-flex justify-content-between">
									<p class = "new-price">Category: <span id="category"></span></p>
									<p class = "new-price">Brand: <span id="brand"></span></p>
								</div>

								<div class = "product-price">
									<p class = "new-price">Stock: <span id="stock"></span></p>
								</div>
								<div class = "product-detail">
									<h3>Description: </h3>
									<p id="description"></p>
								</div>

								<div class = "purchase-info">
									<button type = "button" class = "btn btn-wdanger btnModalOrder" id="btnOrder" >
										Order <i class = "fa fa-shopping-cart"></i>
									</button>
								</div>

							</div>
						</div>
					</div>
			</div>

		</div>
	</div>
</div>

<!-- Modal Form -->
<!-- Modal -->
<div class="modal fade" id="modalForm" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				 <h5 class="modal-title">Form Order</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<form id="formOrder" method="POST">
					<div class="row mb-3">
						<label for="inputEmail3" class="col-sm-3 col-form-label">No Pesanan</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" id="inputEmail3" disabled>
						</div>
					</div>
					<div class="row mb-3">
						<label for="inputPassword3" class="col-sm-3 col-form-label">Tanggal</label>
						<div class="col-sm-6">
							<input type="text" name="tanggal" class="form-control" id="tanggal" readonly>
						</div>
					</div>
					<div class="row mb-3">
						<label for="inputPassword3" class="col-sm-3 col-form-label">Nama Suplier</label>
						<div class="col-sm-8">
							<input type="text" name="name" class="form-control" id="name" placeholder="Name Suplier" required>
						</div>
					</div>
					<div class="row mb-3">
						<label for="inputPassword3" class="col-sm-3 col-form-label">Qty</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" name="qty" maxlength="16" placeholder="QTY" id="qty" pattern="[0-9]+"   oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required>
						</div>
					</div>
					<div class="row mb-3">
						<label for="inputPassword3" class="col-sm-3 col-form-label">Nama Product</label>
						<div class="col-sm-8">
							<input type="text" name="productName" class="form-control" id="productName" readonly>
						</div>
					</div>
					<div class="row mb-3">
						<label for="inputPassword3" class="col-sm-3 col-form-label">Total</label>
						<div class="col-sm-6">
							<input type="text" name="total" class="form-control" id="totalprice" readonly>
						</div>
					</div>
					<button type="submit" class="btn btn-primary"><i class="fa fa-cart"></i> Process Order</button>
				</form>
			</div>

		</div>
	</div>
</div>

<script >
function dateNow(date) {
    const day = String(date.getDate()).padStart(2, '0');
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const year = date.getFullYear();

    return `${day}-${month}-${year}`;
}
function roundToNearestHalf(number) {
  var decimalPart = number - Math.floor(number); // Bagian desimal angka

  if (decimalPart >= 0.5) {
    // Jika bagian desimal lebih besar dari 0,5, maka bulatkan ke 0,5 terdekat
    return Math.floor(number) + 0.5;
  } else {
    // Jika bagian desimal kurang dari atau sama dengan 0,5, maka bulatkan ke bawah
    return Math.floor(number);
  }
}
function updateStarRating(starnum) {
    var starRating = document.getElementById('starRating'); // Dapatkan elemen span dengan ID 'starRating'
    starRating.setAttribute('data-starnum', starnum); // Setel nilai atribut data-starnum sesuai dengan data dari server
}

function linkImg(img){
	return `<img src = "${img}" alt = "image">`;
}

function imgSelect(index,img) {
	return `<div class = "img-item">
			<a href = "#" data-id = "${index}">
				<img src = "${img}" alt = "image">
			</a>
		</div>`
}

function slideSelect(){
	const imgs = document.querySelectorAll('.img-select a');
	const imgBtns = [...imgs];
	let imgId = 1;
	imgBtns.forEach((imgItem) => {
	    imgItem.addEventListener('click', (event) => {
	        event.preventDefault();
	        imgId = imgItem.dataset.id;
	        slideImage(imgId);
	    });
	});
}

function 	getQty() {
	$('#qty').keyup(function(){

		var qty = $('#qty').val();
		return qty;
	})
}

function slideImage(imgId){
    const displayWidth = document.querySelector('.img-showcase img:first-child').clientWidth;

    document.querySelector('.img-showcase').style.transform = `translateX(${- (imgId - 1) * displayWidth}px)`;
}



window.addEventListener('resize', slideImage);


$('#btnShowProduct').on('click', function(){
	
	$('#loadSpiner').removeClass('d-none');

	 setTimeout(function(){
	 	$('#loadSpiner').addClass('d-none');
		getDataApi();
		$('#tableContent').removeClass('d-none');

	 },2000);
});

$('#tbProduct').on('click','.btnShowDetail', function(){
	$('#modalDetail').modal('show');
	let id = $(this).attr('idDetail');
	fetch('https://dummyjson.com/products/'+id)
	.then(res => res.json())
	.then(data=>{
		let images = data.images;
		// let imagesSelect = data.images;

		let rating = roundToNearestHalf(data.rating);
		let imgSlide ='';
		let imageSelect ='';
		  var buttonElement = document.getElementById("btnOrder");

		images.forEach(img=> {
			imgSlide += linkImg(img)
		})
		images.forEach((img, index) => {
	        imageSelect += imgSelect(index + 1, img);
	    })

		$('#nameProduct').html(data.title)
		$('#category').html(data.category)
		$('#price').html('$'+data.price)
		$('#brand').html(data.brand)
		$('#stock').html(data.stock);
		$('#rating').html(data.rating);
		$('#showCase').html(imgSlide);
		$('#imgSelect').html(imageSelect);
		$('#description').html(data.description);
		buttonElement.setAttribute("idproduk", data.id);

		updateStarRating(rating);
		slideSelect();

	});
})

$('#modalDetail').on('click', '.btnModalOrder', function(){
	$('#modalDetail').modal('hide');
	$('#modalForm').modal('show');

	const idProduct = $(this).attr('idproduk');
	fetch('https://dummyjson.com/products/'+idProduct)
	.then(res => res.json())
	.then(data=>{
		$('#productName').val(data.title)
		$('#tanggal').val(dateNow(new Date()))

		
		const price = data.price;

		$('#qty').on('input', function () {
			qty = parseFloat($(this).val());
			const totalPrice = qty * price; 
			if (totalPrice>0) {
				$('#totalprice').val('$'+totalPrice)
			}else{
				$('#totalprice').val('0')
			}
		});

	})
	
})

      $('#formOrder').submit(function(e) {
        e.preventDefault();

          $.ajax({
            method   : 'post',
            url      : '<?php echo base_url();?>order',
            data     : new FormData(this),
            dataType : 'json',
            contentType : false,
            processData : false,
            success:function(response) {
              if(response.status == 1) {
                            Swal.fire({
                              icon: 'success',
                              title: 'Success',
                              timer: 3000,
                              html: '<b>Success</b>',
                            }).then(function() {
                              window.location.href = "<?=base_url()?>";
                            });
                        } 
                        else {
                         	Swal.fire({
                              icon: 'warning',
                              title: 'Failed',
                              timer: 3000,
                              html: '<b>Failed</b>',
                            }).then(function() {
                              window.location.href = "<?=base_url() ?>";
                            });
                        }
                    }
                })
      });

function getDataApi(){
	fetch('https://dummyjson.com/products')
		.then(response => response.json())
		.then(data => {
		    $('#tbProduct').DataTable({
		    responsive: true,
		      data: data.products,
		      columns: [
		       { 
		     	 data: null,
		      		render: function (data, type, row) {
		        	return '<img src="' + row.images[0] + '" width="50px">';
		      		}
		  		}, 
		        { data: 'title' }, 
		        { data: 'category' },
		        { data: 'brand' },
		        { data: 'stock' }, 
		        { data: null,
		      		render: function (data, type, row) {
		        	return `<span>$</span>${data.price}`;
		      		} 
		      	},
		      	{ data: null,
		      		render: function (data, type, row) {
		        	return `<button class="btn btn-warning mb-2 btnShowDetail" idDetail="${data.id}" ><i class='fa fa-eye'></i> View</button>`;
		      		} 
		      	}, 
		      ]
		    });
  		})
  	.catch(error => {
    	console.error('Terjadi kesalahan:', error);
  	});
}
 	
</script>
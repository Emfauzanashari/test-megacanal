<!doctype html>
<html lang="en">
	<head>
		<?=$this->load->view("admin_layout/admin_meta_tag");?>
		<?=$this->load->view("admin_layout/admin_css");?>
	</head>
	
		<body>
		
		<div class="wrapper" id="main-wrapper">
			<header class="topbar">
				<?php $this->load->view("admin_layout/admin_nav_topbar"); ?>
			</header>
			
				<div class="page-wrapper" style="margin-left:0px;">
					<?php $this->load->view($content);?>
				</div>
				<?php $this->load->view("admin_layout/admin_footer"); ?>
		</div>
	</body>
	<?=$this->load->view("admin_layout/admin_js");?>
</html>


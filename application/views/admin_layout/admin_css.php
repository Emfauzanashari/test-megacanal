<link href="<?=base_url(); ?>assets/css/style.css" rel="stylesheet">

	<!-- Bootstrap CSS -->
	<link href="<?=base_url(); ?>assets/bootstrap/bootstrap.min.css" rel="stylesheet">
	<link href="<?=base_url(); ?>assets/datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
	<!-- Dropify -->
	<link href="<?=base_url(); ?>assets/dropify/dropify.min.css" rel="stylesheet">
	<link href="<?=base_url(); ?>assets/datatables/datatables.min.css" rel="stylesheet">


	<!-- fontawesome -->
	<link href="<?=base_url(); ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- Jquery -->
	<script src="<?=base_url(); ?>assets/js/jquery.min.js"></script>


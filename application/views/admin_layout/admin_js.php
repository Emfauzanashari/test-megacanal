<!--end wrapper-->
<!-- Bootstrap JS -->

<script src="<?=base_url(); ?>assets/js/jquery.validate.js"></script>


<!-- Bootstrap JS -->
<script src="<?=base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
<!--plugins-->
<script src="<?=base_url(); ?>assets/datepicker/js/bootstrap-datepicker.min.js"></script>
<!-- Datatables -->
<script src="<?=base_url(); ?>assets/datatables/datatables.min.js"></script>

<!-- datepicker -->
<script src="<?=base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
<!-- dropify -->
<script src="<?=base_url(); ?>assets/dropify/dropify.min.js"></script>
<script src="<?=base_url(); ?>assets/js/app.js"></script>
<script src="<?=base_url(); ?>assets/js/sweetaler.js"></script>
<script src="<?=base_url(); ?>assets/js/votestar.js"></script>



<script>

  $(document).ready(function () { 
    $.validator.addMethod("cek_special_character", function(value, element) {
      var regex = new RegExp(/[~`!#$%\^&*+=\\[\]\\';()\/{}|\\"<>\?]/, 'gi');


      if (regex.test(value)) {
        result = false;
      } else {
        result = true;
      }
      return result;
    });

      $.validator.addMethod("cek_email", function(value, element) {
            $.ajax({
                method : "post",
                url    : '<?=base_url().'signup/cek_email'?>',
                data   : { value:value },
                success: function(response) {
                    if(response == 1) {
                        result = true;
                    } else {
                        result = false;
                    }
                },
                async: false
            });
            return result;
        });

$.validator.addMethod("cek_email_forgot", function(value, element) {
        $.ajax({
          method : "post",
          url    : '<?=base_url().'signup/cek_email'?>',
          data   : { value:value },
          success: function(response) {
            if(response == 0) {
              result = true;
            } else {
              result = false;
            }
          },
          async: false
        });
        return result;
      });

      var validateForgot = $("#formForgot").validate({
        rules: {
          
          email: {
            required: true,
            email: true,
            cek_email_forgot: true
          },
        },
        messages: {
          email: {
            required: "Email is Required.",
            email: "Input Email with true.",
            cek_email_forgot: "Your Email Unregistered."
          },
        },
        errorElement: "em",
        errorClass: "has-error",
        highlight: function(element, errorClass) {
          $(element).parent().addClass('has-error')
          $(element).addClass('has-error')
        },
        unhighlight: function(element, errorClass) {
          $(element).parent().removeClass('has-error')
          $(element).removeClass('has-error')
        },
        errorPlacement: function(error, element) {
          if(element.parent('.input-group').length) {
            error.insertAfter(element.parent('.input-group'));
          } else {
            error.insertAfter(element);
          }
        }
      });

      $('#formForgot').submit(function(e) {
        e.preventDefault();
        if (validateForgot.valid()) {

          $.ajax({
            method   : 'post',
            url      : '<?php echo base_url();?>cekdata',
            data     : new FormData(this),
            dataType : 'json',
            contentType : false,
            processData : false,
            beforeSend: function() {
              $('.loading i').addClass('spinner-border');
              $('#submit').addClass('disabled');
            },
            success:function(response) {
              if(response.status == 1) {
                            // $("#formForgot")[0].reset();
                            Swal.fire({
                              icon: 'success',
                              title: 'Success',
                              timer: 3000,
                              html: '<b>Success</b> Send Link Change Password<br><p>Check Your Email </p><br><p>Link Sent in your Email</p>',
                            }).then(function() {
                              window.location.href = "<?=base_url().'login' ?>";
                            });
                        } 
                        else {
                          $('#feedback-register').html(
                            '<div class="alert alert-danger animated fadeIn" role="alert">'+
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><i class="close-icon"></i></button>'+
                            '<span>'+response.message+'</span>'+
                            '</div>'
                            );
                        }
                    }
                })
        }
      });

      var validateChange = $("#changePass").validate({
            rules: {
               
                password: {
                    minlength: 6,
                    required: true
                },
                retypepass: {
                    required: true,
                    equalTo : "#inputPassword"
                },
            },
            messages: {
                 password: {
                    minlength: "Password MIN 6 characters",
                    required: "Password is Required."
                },
                retypepass: {
                    required: "Retype Password is Required.",
                    equalTo: "Password Not Match"
                },
            },
            errorElement: "em",
            errorClass: "has-error",
            highlight: function(element, errorClass) {
                $(element).parent().addClass('has-error')
                $(element).addClass('has-error')
            },
            unhighlight: function(element, errorClass) {
                $(element).parent().removeClass('has-error')
                $(element).removeClass('has-error')
            },
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent('.input-group'));
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $('#changePass').submit(function(e) {
            e.preventDefault();
            if (validateChange.valid()) {

                $.ajax({
                    method   : 'post',
                    url      : '<?php echo base_url();?>change-pass',
                    data     : new FormData(this),
                    dataType : 'json',
                    contentType : false,
                    processData : false,
                    success:function(response) {
                        if(response.status == 1) {
                            // $("#formForgot")[0].reset();
                            Swal.fire({
                      icon: 'success',
                      title: 'Success',
                      timer: 3000,
                      html: '<b>Success</b> Change Password',
                    }).then(function() {
                      window.location.href = "<?=base_url().'login' ?>";
                    });
                        } 
                        else {
                            $('#feedback-register').html(
                              '<div class="alert alert-danger animated fadeIn" role="alert">'+
                                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><i class="close-icon"></i></button>'+
                                '<span>'+response.message+'</span>'+
                              '</div>'
                            );
                        }
                    }
                })
            }
        });

      var validateSignup = $("#form-register").validate({
            rules: {
                name: {
                    required: true,
                    cek_special_character: true
                },
                email: {
                    required: true,
                    email: true,
                    cek_email: true
                },
                birthday: {
                    required: true,
                },
                phone: {
                    required: true,
                    maxlength: 14,
                    minlength:8,
                },
                password: {
                    minlength: 6,
                    required: true
                },
                retypepass: {
                    required: true,
                    equalTo : "#inputPassword"
                },
                address:{
                    required: true,
                    cek_special_character:true
                },
                 image:{
                    required: true,
                },
            },
            messages: {
                name: {
                    required: "Name is Required.",
                    cek_special_character: "Names cannot contain special characters."
                },
                email: {
                    required: "Email is Required.",
                    email: "Input Email with true.",
                    cek_email: "Email already registered."
                },
                birthday: {
                    required: "Birthday is Required.",
                },
                phone: {
                    required: "Phone is Required.",
                    maxlength: "Phone Number Max 14 characters.",
                    minlength: "Phone Number Min 8 characters",
                },
                password: {
                    minlength: "Password MIN 6 characters",
                    required: "Password is Required."
                },
                retypepass: {
                    required: "Retype Password is Required.",
                    equalTo: "Password Not Match"
                },
                address: {
                    required :"Address is Required",
                    cek_special_character: "Address cannot contain special characters"
                }
            },
            errorElement: "em",
            errorClass: "has-error",
            highlight: function(element, errorClass) {
                $(element).parent().addClass('has-error')
                $(element).addClass('has-error')
            },
            unhighlight: function(element, errorClass) {
                $(element).parent().removeClass('has-error')
                $(element).removeClass('has-error')
            },
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent('.input-group'));
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $('#form-register').submit(function(e) {
            e.preventDefault();
            if (validateSignup.valid()) {

                $.ajax({
                    method   : 'post',
                    url      : '<?php echo base_url();?>signup/insert',
                    data     : new FormData(this),
                    dataType : 'json',
                    contentType : false,
                    processData : false,
                    success:function(response) {

                        if(response.status == 1) {
                            $("#form-register")[0].reset();
                            top.location.href="<?php echo base_url();?>login";
                        } 
                        else {
                            $('#feedback-register').html(
                              '<div class="alert alert-danger animated fadeIn" role="alert">'+
                                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><i class="close-icon"></i></button>'+
                                '<span>'+response.message+'</span>'+
                              '</div>'
                            );
                        }
                    }
                })
            }
        });


    var validateProfil = $("#formProfile").validate({
      rules: {
        name: {
          required: true,
          cek_special_character: true
        },

        phone: {
          required: true,
          maxlength: 14,
          minlength:8,
        },
        address:{
          required: true,
          cek_special_character:true
        },
      },
      messages: {
        name: {
          required: "Name is Required.",
          cek_special_character: "Names cannot contain special characters."
        },

        phone: {
          required: "Phone is Required.",
          maxlength: "Phone Number Max 14 characters.",
          minlength: "Phone Number Min 8 characters",
        },
        address: {
          required :"Address is Required",
          cek_special_character: "Address cannot contain special characters"
        }
      },
      errorElement: "em",
      errorClass: "has-error",
      highlight: function(element, errorClass) {
        $(element).parent().addClass('has-error')
        $(element).addClass('has-error')
      },
      unhighlight: function(element, errorClass) {
        $(element).parent().removeClass('has-error')
        $(element).removeClass('has-error')
      },
      errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) {
          error.insertAfter(element.parent('.input-group'));
        } else {
          error.insertAfter(element);
        }
      }
    });
    $('#formProfile').on('submit', function(e){
      e.preventDefault();
      if (validateProfil.valid()) {
         $.ajax({
          method   : 'post',
          url      : '<?php echo base_url();?>profile/updateData',
          data     : new FormData(this),
          dataType : 'json',
          contentType : false,
          processData : false,
          success:function(response) {

            if(response.status == 1) {
              Swal.fire({
                icon: 'success',
                title: 'Success',
                timer: 1500,
                html: '<b>Success</b> Update Data',
              }).then(function() {
                location.reload();
              });

            } 
            else {
              $('#feedback-register').html(
                '<div class="alert alert-danger animated fadeIn" role="alert">'+
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><i class="close-icon"></i></button>'+
                '<span>'+response.message+'</span>'+
                '</div>'
                );
            }
          }
        })
      }
     
    })

var validatePass = $("#formUpdatePass").validate({
      rules: {
       password: {
        minlength: 6,
        required: true
        },
        retypepass: {
          required: true,
          equalTo : "#inputPassword"
        },
      },
      messages: {
        password: {
            minlength: "Password MIN 6 characters",
            required: "Password is Required."
        },
        retypepass: {
            required: "Retype Password is Required.",
            equalTo: "Password Not Match"
        },
      },
      errorElement: "em",
      errorClass: "has-error",
      highlight: function(element, errorClass) {
        $(element).parent().addClass('has-error')
        $(element).addClass('has-error')
      },
      unhighlight: function(element, errorClass) {
        $(element).parent().removeClass('has-error')
        $(element).removeClass('has-error')
      },
      errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) {
          error.insertAfter(element.parent('.input-group'));
        } else {
          error.insertAfter(element);
        }
      }
    });
    $('#formUpdatePass').on('submit', function(e){
      e.preventDefault();
      if (validatePass.valid()) {
        $.ajax({
          method   : 'post',
          url      : '<?php echo base_url();?>profile/updatePassword',
          data     : new FormData(this),
          dataType : 'json',
          contentType : false,
          processData : false,
          success:function(response) {

            if(response.status == 1) {
              Swal.fire({
                icon: 'success',
                title: 'Success',
                timer: 1500,
                html: '<b>Success</b> Update Data',
              }).then(function() {
                window.location.href = "<?=base_url().'logout' ?>";
              });

            } 
            else {
              $('#feedback-register').html(
                '<div class="alert alert-danger animated fadeIn" role="alert">'+
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><i class="close-icon"></i></button>'+
                '<span>'+response.message+'</span>'+
                '</div>'
                );
            }
          }
        })
      }
      
    })

  });
</script>

<script>
  $('.dropify').dropify();

$('#staticBackdrop').on('click', function(){
  console.log("kjhgfd")
  $('#modalUpdateImg').modal('show');
})
    $('#formUpdateImg').on('submit', function(e){
        e.preventDefault();

        // var formImg = $('#formUpdateImg')[0];
        //  var dataEdit = new FormData(formImg);
        $.ajax({
        url : '<?=base_url().'profile/updateImage'?>',
        processData: false,
            contentType: false,
        type: 'POST', 
        dataType : 'json',      
        data: new FormData(this),
        success : function (response){
          var a= response.status;
          console.log(a);
              $('#modalUpdateImg').modal('hide');
               if(response.status == 1) {
            Swal.fire({
              icon: 'success',
              title: 'Success',
              timer: 1500,
              html: '<b>Success</b> Update Data',
            }).then(function() {
              location.reload();

            });

          }
        }

    });
    });

    $('#btnLogout').on('click', function(){
        window.location.href = "<?=base_url().'logout' ?>";
    })
</script>

<script>
    $(document).ready(function () {
  //Password show & hide js

      $(".show_hide_password a").on('click', function (event) {
                event.preventDefault();
                if ($('.show_hide_password input').attr("type") == "text") {

                    $('.show_hide_password input').attr('type', 'password');
                    $('.show_hide_password i').removeClass("fa fa-eye");
                    $('.show_hide_password i').addClass("fa fa-eye-slash");
                } else if ($('.show_hide_password input').attr("type") == "password") {
                    $('.show_hide_password input').attr('type', 'text');
                    $('.show_hide_password i').removeClass("fa fa-eye-slash");
                    $('.show_hide_password i').addClass("fa fa-eye");
                }
            });
        $(".show_hide_password_retype a").on('click', function (event) {
                event.preventDefault();
                if ($('.show_hide_password_retype input').attr("type") == "text") {

                    $('.show_hide_password_retype input').attr('type', 'password');
                    $('.show_hide_password_retype i').removeClass("fa fa-eye");
                    $('.show_hide_password_retype i').addClass("fa fa-eye-slash");
                } else if ($('.show_hide_password_retype input').attr("type") == "password") {
                    $('.show_hide_password_retype input').attr('type', 'text');
                    $('.show_hide_password_retype i').removeClass("fa fa-eye-slash");
                    $('.show_hide_password_retype i').addClass("fa fa-eye");
                }
            });




      $("#show_hide_password a").on('click', function (event) {
        event.preventDefault();
        if ($('#show_hide_password input').attr("type") == "text") {

          $('#show_hide_password input').attr('type', 'password');
          $('#show_hide_password i').removeClass("fa fa-eye");
          $('#show_hide_password i').addClass("fa fa-eye-slash");
        } else if ($('#show_hide_password input').attr("type") == "password") {
          $('#show_hide_password input').attr('type', 'text');
          $('#show_hide_password i').removeClass("fa fa-eye-slash");
          $('#show_hide_password i').addClass("fa fa-eye");
        }
      });
    });


  </script>



<script>
  $('.birthday').datepicker({
    endDate: '+1d',
    datesDisabled: '+1d',
    todayHighlight: true,
    autoclose: true,
  });
</script>

<!-- Sweetalert -->
<?php if($this->session->flashdata('warning')): ?>
<script>  
 Swal.fire({
  icon: 'error',
  title: 'warning',
  timer: 2000,
  html: '<b>Warning</b> - <?=$this->session->flashdata("warning");?>'
})
</script>
<?php endif; ?>
<?php if($this->session->flashdata('warning_forgot')): ?>
  <script>  
   Swal.fire({
    icon: 'error',
    title: 'Warning',
    timer: 2000,
    html: '<b>Warning</b> - <?=$this->session->flashdata("warning_forgot");?>'
  }).then(function() {
    window.location.href = "<?=base_url().'logout' ?>";
  });
</script>
<?php endif; ?>
<?php if($this->session->flashdata('success')): ?>
  <script>  
   Swal.fire({
    icon: 'success',
    title: 'Success',
    timer: 1500,
    html: '<b>Success</b> - <?=$this->session->flashdata("success");?>'
  })
</script>
<?php endif; ?>

<?php if($this->session->flashdata('warning')): ?>
  <script>  
   Swal.fire({
    icon: 'Warning',
    title: 'Warnung',
    timer: 1500,
    html: '<b>Warning</b> - <?=$this->session->flashdata("warning");?>'
  })
</script>
<?php endif; ?>



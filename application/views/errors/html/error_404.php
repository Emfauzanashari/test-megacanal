<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
	<head>	
		<meta charset="utf-8">	
		<title>404 Page Not Found</title>	
		<style type="text/css">		
			::-moz-selection { background: #000000; color: #ffffff; }		
			::selection { background: #000000; color: #ffffff; }		
			#container { position: relative; overflow: hidden; height: 100vh; display: flex; align-items: center; justify-content: center; 
			}		
				.wrap-content { text-align: center; }		
				.btn { position: relative; font-weight: 600; font-size: 16px; background: #000000; color: #ffffff; text-decoration: none; padding: 8px 25px; border-radius: 20px; margin-top: 20px; 
			}		
			code {			
				font-family: Consolas, Monaco, Courier New, Courier, monospace;			
				font-size: 12px;			
				background-color: #f9f9f9;			
				border: 1px solid #D0D0D0;			
				color: #002166;			
				display: block;			
				margin: 14px 0 14px 0;			
				padding: 12px 10px 12px 10px;		
			}		
			img { max-width: 85%; }		
			h5 { font-weight: 900; font-size: 28px; color: #0e5a96; margin-bottom: 10px; 
				}
			h1 { font-weight: 900; font-size: 150px; color: #2196f3; margin-bottom: 10px; 
				}		
			p { font-weight: 600; font-size: 16px; margin-bottom: 20px; 
			}	
		</style>
	</head>
	<body>	
		<div id="container">		
			<div class="wrap-content">	
				<h1>404</h1>		
				<h5>Ehh, mau nyari apa nih?</h1>			
				<p>Halaman yang dicari gak ada tuh, silahkan cek lagi ya</p>			
				<a href="javascript:void(0)" onclick="history.back()" class="btn">Back to home</a>		
			</div>	
		</div>
	</body>
</html>
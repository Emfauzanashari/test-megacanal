<?php
	$random 					= random_string('alnum',7);
	$config['allowed_types'] 	= 'gif|jpg|png|jpeg|JPG|JPEG|PNG|svg'; //extension yang diperbolehkan untuk diupload
	$config['file_name']		= 'img_'.$random;
	$config['remove_space']		= 'TRUE';
	$config['overwrite']		= 'TRUE';
	$config['max_size']			= '2000';
	$config['max_width']		= '3100';
	$config['max_height']		= '3100';
?>
-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 07 Sep 2023 pada 11.13
-- Versi server: 10.4.19-MariaDB
-- Versi PHP: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `order_list`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` text NOT NULL,
  `timestamp` varchar(120) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `user_agent`, `last_activity`, `data`, `timestamp`) VALUES
('66q3vroehsnksmolcmi7rjv6s1sdr2n3', '::1', '', 0, '__ci_last_regenerate|i:1694077680;', '1694077975'),
('6jld1rjdtf10kdklsdqhfl67731qr6fp', '::1', '', 0, '', '1687623489'),
('f7e4i5vkgv5n7ur4esfh9br90231c1oq', '::1', '', 0, '__ci_last_regenerate|i:1687537987;', '1687537991'),
('hl7egrd2cs198qsjea10t21999t8f4p0', '192.168.1.75', '', 0, '__ci_last_regenerate|i:1687537999;', '1687538095'),
('iaccntn6q9vjioegmtvfhbnsf2u2pg0e', '192.168.1.75', '', 0, '__ci_last_regenerate|i:1687672047;', '1687672047'),
('jo0lnv8d9ip7l538g1r5gt2evnsl6sc7', '::1', '', 0, '__ci_last_regenerate|i:1694071451;', '1694071451'),
('llkd8krge4jt8svaj32drdb5t9ja816s', '192.168.1.75', '', 0, '__ci_last_regenerate|i:1687683467;', '1687683467'),
('sg6c21p2jnmmpql0kudvt096napjrbbr', '::1', '', 0, '__ci_last_regenerate|i:1687599896;username|s:0:\"\";name|s:4:\"test\";userid|s:32:\"dkt6V1U3UGpnWXk1THcyamljTHlZQT09\";is_login|b:1;email|s:14:\"test@gmail.com\";', '1687600036'),
('spagq66jm5r27u4p41g8u0c2m14ua290', '::1', '', 0, '__ci_last_regenerate|i:1687628972;', '1687628974'),
('ugfa1pleiplri1mimadsaq9f7un4airh', '::1', '', 0, '__ci_last_regenerate|i:1687671718;', '1687671972');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pesanan`
--

CREATE TABLE `pesanan` (
  `id` int(11) NOT NULL,
  `no_pesanan` varchar(20) NOT NULL,
  `nm_suplier` varchar(50) NOT NULL,
  `nm_produk` varchar(50) NOT NULL,
  `qty` float NOT NULL,
  `total` float NOT NULL,
  `tanggal` varchar(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pesanan`
--

INSERT INTO `pesanan` (`id`, `no_pesanan`, `nm_suplier`, `nm_produk`, `qty`, `total`, `tanggal`, `created_date`) VALUES
(1, '1031798067', 'Ronald', 'iPhone 9', 1, 549, '07-09-2023', '2023-09-07 16:11:29');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indeks untuk tabel `pesanan`
--
ALTER TABLE `pesanan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `pesanan`
--
ALTER TABLE `pesanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
